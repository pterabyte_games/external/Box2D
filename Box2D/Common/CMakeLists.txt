project (Common)

add_library (
	${PROJECT_NAME}

	OBJECT

	b2BlockAllocator.cpp
	b2BlockAllocator.h
	b2Draw.cpp
	b2Draw.h
	b2GrowableStack.h
	b2Math.cpp
	b2Math.h
	b2Settings.cpp
	b2Settings.h
	b2StackAllocator.cpp
	b2StackAllocator.h
	b2Timer.cpp
	b2Timer.h
)

target_compile_features (
	${PROJECT_NAME}
	PRIVATE
	cxx_std_11
)

set_target_properties (
	${PROJECT_NAME}
	PROPERTIES
		POSITION_INDEPENDENT_CODE ON
)

add_library (
	Box2D::${PROJECT_NAME}
	ALIAS ${PROJECT_NAME}
)

